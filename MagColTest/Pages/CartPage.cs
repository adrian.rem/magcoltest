﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MagColTest.Pages
{
    public class CartPage
    {
        IWebDriver driver;

        public CartPage(IWebDriver driver)
        {
            this.driver = driver;
        }

        IWebElement AddToCrtSccss => driver.FindElement(By.XPath("//*[@id=\"cart - form\"]/div/div/div/ul/li/ul/li/span"));

        IReadOnlyCollection<IWebElement> CrtItmsTtl => driver.FindElements(By.ClassName("text-14"));

        IReadOnlyCollection<IWebElement> CrtItmsRmv => driver.FindElements(By.ClassName("cart-remove"));

        IReadOnlyCollection<IWebElement> CrtItmsQty => driver.FindElements(By.XPath("//*[@title=\"Cantitate\"]"));

        IWebElement PrcdChckOut => driver.FindElement(By.ClassName("btn-proceed-checkout"));

        public string ItemAddedToCart()
        {
            return AddToCrtSccss.Text;
        }

        public string GetItemName(int ItemNumber)
        {
            List<IWebElement> test = CrtItmsTtl.ToList();
            return test[ItemNumber].Text;
        }

        public void RemoveItemFromCart(int ItemNumber)
        {
            List<IWebElement> test = CrtItmsRmv.ToList();
            test[ItemNumber].Click();
        }

        public void SetItemQuantity(int ItemNumber)
        {
            List<IWebElement> test = CrtItmsQty.ToList();
            test[ItemNumber].Click();
        }

        public void PlaceOrder()
        {
            PrcdChckOut.Click();
        }


    }
}
