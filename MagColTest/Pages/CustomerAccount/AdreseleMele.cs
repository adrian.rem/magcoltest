﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MagColTest.Pages.CustomerAccount
{
    public class AdreseleMele
    {
        IWebDriver driver;

        public AdreseleMele(IWebDriver driver)
        {
            this.driver = driver;
        }

        // Go to 'Adresele Mele' option
        IWebElement SelOption => driver.FindElement(By.LinkText("Adresele mele"));

        // 'Date Contact' section
        IWebElement Fname => driver.FindElement(By.Id("firstname"));
        IWebElement Lname => driver.FindElement(By.Id("lastname"));
        IWebElement Comp => driver.FindElement(By.Id("company"));
        IWebElement Tel => driver.FindElement(By.Id("telephone"));
        IWebElement Fx => driver.FindElement(By.Id("fax"));

        // If details are already inserted, click this link to edit the defails
        IWebElement EdtAddrs => driver.FindElement(By.LinkText("Schimba adresa de facturare"));

        // 'Adresa' section
        IWebElement Str1 => driver.FindElement(By.Id("street_1"));
        IWebElement Str2 => driver.FindElement(By.Id("street_2"));
        IWebElement Cty => driver.FindElement(By.Id("city"));
        IWebElement Reg => driver.FindElement(By.Id("region_id"));
        IWebElement Zp => driver.FindElement(By.Id("zip"));
        IWebElement Cntry => driver.FindElement(By.Id("country"));

        // Save button
        IWebElement Sve => driver.FindElement(By.CssSelector("#form-validate > div.buttons-set > button"));

        // Success messages
        IWebElement SccssSve => driver.FindElement(By.XPath("/html/body/div[2]/div/div/div[3]/div/div/div/div[1]/div/ul/li/ul/li/span"));
        IWebElement SccssGglLgn => driver.FindElement(By.XPath("/html/body/div[2]/div/div/div[3]/div/div/div/div[1]/ul/li/ul/li/span"));


        public void SelectOption()
        {
            SelOption.Click();

            if (EdtAddrs.Displayed)
            {
                EdtAddrs.Click();
            }
        }

        public void DateContact(
            string Prenume, 
            string Nume,
            string DenSocietate,
            int Telefon,
            int Fax)
        {
            Fname.Clear();
            Fname.SendKeys(Prenume);
            Lname.Clear();
            Lname.SendKeys(Nume);
            Comp.Clear();
            Comp.SendKeys(DenSocietate);
            Tel.Clear();
            Tel.SendKeys(Telefon.ToString());
            Fx.Clear();
            Fx.SendKeys(Fax.ToString());
        }

        public void Adresa(
            string Adresa1,
            string Adresa2,
            string Localitate,
            string Judet,
            string CodPostal,
            string Tara)
        {
            Str1.Clear();
            Str1.SendKeys(Adresa1);
            Str2.Clear();
            Str2.SendKeys(Adresa2);
            Cty.Clear();
            Cty.SendKeys(Localitate);
            Reg.SendKeys(Judet);
            Zp.Clear();
            Zp.SendKeys(CodPostal);
            Cntry.SendKeys(Tara);
        }

        public void SaveDetails()
        {
            Sve.Submit();
        }

        public string SuccessMessage_Save()
        {
            string sm = SccssSve.Text;
            return sm;
        }

        public string SuccessMessage_GoogleLogin()
        {
            string sm = SccssGglLgn.Text;
            return sm;
        }


    }
}
