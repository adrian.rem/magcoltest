﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MagColTest.Other.DataMaps
{
    public class InfoMessages
    {

        [JsonProperty("GoogleLoginSuccess")]
        public string GoogleLoginSuccess { get; set; }

        [JsonProperty("AddressSaveSuccess")]
        public string AddressSaveSuccess { get; set; }
    }

    public class ErrorMessages
    {

        [JsonProperty("a")]
        public string A { get; set; }

        [JsonProperty("x")]
        public string X { get; set; }
    }

    public class AddOrEditAdreseleMele
    {

        [JsonProperty("infoMessages")]
        public InfoMessages InfoMessages { get; set; }

        [JsonProperty("errorMessages")]
        public ErrorMessages ErrorMessages { get; set; }
    }

    public class Test2
    {

        [JsonProperty("a")]
        public string A { get; set; }

        [JsonProperty("c")]
        public string C { get; set; }
    }

    public class MessageData
    {

        [JsonProperty("AddOrEditAdreseleMele")]
        public AddOrEditAdreseleMele AddOrEditAdreseleMele { get; set; }

        [JsonProperty("test2")]
        public Test2 Test2 { get; set; }
    }
}
