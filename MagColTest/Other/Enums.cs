﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MagColTest.Other
{
    class Enums
    {
        public enum Browsers
        {
            Firefox,
            Chrome,
            ChromeMobile,
            Edge
        }
    }
}
