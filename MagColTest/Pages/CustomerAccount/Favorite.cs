﻿using AventStack.ExtentReports;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MagColTest.Pages.CustomerAccount
{
    public class Favorite
    {
        IWebDriver driver;

        public Favorite(IWebDriver driver)
        {
            this.driver = driver;
        }

        // Go to 'Informatii Cont' option
        IWebElement SelOption => driver.FindElement(By.LinkText("Favorite"));

        IWebElement AddSccssMsg => driver.FindElement(By.XPath("/html/body/div[2]/div/div/div[3]/div/div/div/div[1]/div/div[1]/ul/li/ul/li/span"));

        // Remove from favorites button
        ReadOnlyCollection<IWebElement> RmvBtn => driver.FindElements(By.ClassName("btn-remove2"));

        IList<IWebElement> FavLst => driver.FindElements(By.TagName("h3"));

        IWebElement WshLstEmpty => driver.FindElement(By.ClassName("wishlist-empty"));

        public void SelectOption()
        {
            SelOption.Click();
        }

        /// <summary>
        /// To remove item from favorites list, you need to specify the item number
        /// </summary>
        /// <param name="ItemNumber">Item number on the list, 0 being the first one on the favorites list</param>
        public void RemoveFromFavoriteList(int ItemNumber)
        {
            List<IWebElement> test = RmvBtn.ToList();
            test[ItemNumber].Click();
        }

        public string SuccessMessage()
        {
            return AddSccssMsg.Text;
        }

        public string FavoritesList(int ItemNumber)
        {
            List<IWebElement> test = FavLst.ToList();
            return test[ItemNumber].Text;
        }

        public string WhishListEmptyMessage()
        {
            return WshLstEmpty.Text;
        }
    }
}
