﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace MagColTest.Pages
{
    class SearchResults
    {
        IWebDriver driver;

        public SearchResults(IWebDriver driver)
        {
            this.driver = driver;
        }

        // Search box
        IWebElement SrchBx => driver.FindElement(By.Id("search"));

        // Seached items
        ReadOnlyCollection<IWebElement> RsltItms => driver.FindElements(By.ClassName("item"));

        ReadOnlyCollection<IWebElement> RsltItmsTtl => driver.FindElements(By.ClassName("a-left"));

        IWebElement ItmsAmnt => driver.FindElement(By.ClassName("amount"));
        
        public void SearchAndSubmit(string SearchInput)
        {
            SrchBx.SendKeys(SearchInput);
            SrchBx.SendKeys(Keys.Enter);
        }

        public void SelectItem(int ItemNumber)
        {
            List<IWebElement> test = RsltItmsTtl.ToList();
            test[ItemNumber].Click();
        }

        public string ProductName(int ItemNumber)
        {
            List<IWebElement> test = RsltItmsTtl.ToList();
            return test[ItemNumber].Text;
        }

        public int SeachItemCount()
        {
            int CountItems = RsltItms.Count;
            return CountItems;
        }

        public int PageSearchItemCount()
        {
            int PageCountItems = 0;
            string Result = ItmsAmnt.Text;

            var resultString = Regex.Match(Result, @"\d+").Value;
            PageCountItems = Int32.Parse(resultString);

            return PageCountItems;
        }

    }


}
