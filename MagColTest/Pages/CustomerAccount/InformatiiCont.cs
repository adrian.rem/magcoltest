﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MagColTest.Pages.CustomerAccount
{
    public class InformatiiCont
    {
        IWebDriver driver;

        public InformatiiCont(IWebDriver driver)
        {
            this.driver = driver;
        }

        // Go to 'Informatii Cont' option
        IWebElement SelOption => driver.FindElement(By.LinkText("Informatii Cont"));

        // 'Informatii Cont' section
        IWebElement Fname => driver.FindElement(By.Id("firstname"));
        IWebElement Lname => driver.FindElement(By.Id("lastname"));
        IWebElement Eml => driver.FindElement(By.Id("email"));
        IWebElement ChngPswd => driver.FindElement(By.Id("change_password"));

        // 'Schimba Parola' section
        IWebElement CurPswd => driver.FindElement(By.Id("current_password"));
        IWebElement NwPswd => driver.FindElement(By.Id("password"));
        IWebElement CnfNwPswd => driver.FindElement(By.Id("confirmation"));

        // Save button
        IWebElement Sve => driver.FindElement(By.CssSelector("#form-validate > div.buttons-set > button"));

        // Success messages
        IWebElement SccssSve => driver.FindElement(By.XPath("/html/body/div[2]/div/div/div[3]/div/div/div/div[1]/div/div/ul/li/ul/li/span"));

        public void SelectOption()
        {
            SelOption.Click();
        }

        public void InfoCont(
            string Prenume,
            string Nume,
            string Email
            )
        {
            Fname.Clear();
            Fname.SendKeys(Prenume);
            Lname.Clear();
            Lname.SendKeys(Nume);
            Eml.Clear();
            Eml.SendKeys(Email);
        }

        public void SchimbaParola(
            string Actuala,
            string Noua,
            string ConfirmNoua)
        {
            CurPswd.SendKeys(Actuala);
            NwPswd.SendKeys(Noua);
            CnfNwPswd.SendKeys(ConfirmNoua);
        }

        public void SaveDetails()
        {
            Sve.Submit();
        }

        public string SuccessMessage_Save()
        {
            string sm = SccssSve.Text;
            return sm;
        }
    }
}
