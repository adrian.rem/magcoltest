﻿using MagColTest.Other.DataMaps;
using Newtonsoft.Json;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Edge;
using OpenQA.Selenium.Firefox;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static MagColTest.Other.Enums;

namespace MagColTest.Other
{
    class Utils
    {
        public static IWebDriver GetDriver(Browsers driverType)

        {
            IWebDriver driver = null;

            switch (driverType)
            {
                case Browsers.Firefox:
                    {
                        return new FirefoxDriver();
                    }
                case Browsers.Chrome:
                    {
                        ChromeOptions co = new ChromeOptions();
                        co.AddArgument("start-maximized");

                        return new ChromeDriver(co);
                    }
                case Browsers.ChromeMobile:
                    {
                        ChromeMobileEmulationDeviceSettings cmed = new ChromeMobileEmulationDeviceSettings();
                        cmed.UserAgent = "Mozilla/5.0 (iPhone; CPU iPhone OS 10_3_1 like Mac OS X) AppleWebKit/603.1.30 (KHTML, like Gecko) Version/10.0 Mobile/14E304 Safari/602.1";
                        cmed.Width = 480;
                        cmed.Height = 768;

                        ChromeOptions co = new ChromeOptions();
                        // co.AddArgument("--headless");
                        co.EnableMobileEmulation(cmed);

                        return new ChromeDriver(co);
                    }
                case Browsers.Edge:
                    {
                        return new EdgeDriver();
                    }
                default:
                    {
                        Console.WriteLine("bla bla default");
                        break;
                    }
            }

            return driver;
        }

        public static string msConString = String.Format("Server={0};Database={1};Integrated Security={2}",
                 Properties.Params.Default.MsSqlServer,
                 Properties.Params.Default.MsSqlDbName,
                 Properties.Params.Default.MsSecurity);

        public static DataTable GetDataTable(string query, string tableName)
        {
            DataTable tbl;
            using (SqlConnection conn = new SqlConnection(msConString))
            {
                SqlDataAdapter sda = new SqlDataAdapter(query, conn);
                DataSet ds = new DataSet(tableName);
                sda.Fill(ds, tableName);
                tbl = ds.Tables[tableName];
            }
            return tbl;
        }

        public static MessageData DeserializeJson(string json_data)
        {
            if (!string.IsNullOrEmpty(json_data))
            {
                return JsonConvert.DeserializeObject<MessageData>(json_data);
            }
            return new MessageData();
        }
    }
}
