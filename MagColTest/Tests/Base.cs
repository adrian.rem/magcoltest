﻿using AventStack.ExtentReports;
using AventStack.ExtentReports.Reporter;
using MagColTest.Other;
using MagColTest.Other.DataMaps;
using NUnit.Framework;
using NUnit.Framework.Interfaces;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Props = MagColTest.Properties.Params;

namespace MagColTest.Tests
{
    [TestFixture]
    public class Base
    {
        public static IWebDriver driver;
        public static SqlConnection conn;

        // ExtentReport stuff
        protected ExtentReports extent;
        protected ExtentTest test;
        public string dir;
        public string fileName;

        public Status MapStatuses(TestStatus status)
        {
            switch (status)
            {
                case TestStatus.Inconclusive:
                    return Status.Warning;
                case TestStatus.Skipped:
                    return Status.Skip;
                case TestStatus.Passed:
                    return Status.Pass;
                case TestStatus.Warning:
                    return Status.Warning;
                case TestStatus.Failed:
                    return Status.Fail;
                default:
                    return Status.Fatal;
            }
        }
        // End of ExtentReport stuff

        [OneTimeSetUp]
        public void BeforeAll()
        {
            dir = TestContext.CurrentContext.TestDirectory;
            fileName = this.GetType().ToString();

            // Set up ExtentReport
            ExtentHtmlReporter htmlReporter = new ExtentHtmlReporter(String.Format("{0}\\{1}.html", dir, fileName));
            extent = new ExtentReports();
            extent.AttachReporter(htmlReporter);
        }

        [SetUp]
        public void SetUpTests()
        {
            Console.WriteLine("Setup test!");
            driver = Utils.GetDriver(Enums.Browsers.Chrome);

            // Default navigation page
            driver.Navigate().GoToUrl(Props.Default.SiteName);

            // ExtentReport
            test = extent.CreateTest(TestContext.CurrentContext.Test.Name);

            // Read JSON file
            string json_data = "";
            using (StreamReader sr = new StreamReader(@"C:\Users\jucan\source\repos\MagColTest\MagColTest\TestData\TestMessages.json"))
            {
                json_data = sr.ReadToEnd();
            }
            MessageData md = Utils.DeserializeJson(json_data);
        }

        [SetUp]
        public void DbSetup()
        {
            try
            {
                conn = new SqlConnection(Utils.msConString);
                conn.Open();
            }
            catch (SqlException sqle)
            {
                Console.WriteLine(sqle.Message);
            }
        }



        [TearDown]
        public void TearDownTests()
        {
            Console.WriteLine("Teardown test!");
            driver.Quit();

            // ExtentReport test status
            TestStatus status = TestContext.CurrentContext.Result.Outcome.Status;
            string stk = TestContext.CurrentContext.Result.StackTrace;

            stk = string.IsNullOrEmpty(stk) ? "" : String.Format("{0}", stk);

            Status logstatus = MapStatuses(status);
            test.Log(logstatus, "Test ended with status " + logstatus + stk);
            extent.Flush();
        }

        [OneTimeTearDown]
        public void AfterAll()
        {
            extent.Flush();
        }

    }
}


