﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MagColTest.Pages
{
    public class LoginPage
    {
        IWebDriver driver;

        public LoginPage(IWebDriver driver)
        {
            this.driver = driver;
        }

        IWebElement LoginBut => driver.FindElement(By.LinkText("Login"));

        // To be moved in a separate GoogleLogin page
        IWebElement GoogLogIn => driver.FindElement(By.ClassName("buttonText"));
        IWebElement GoogEml => driver.FindElement(By.Name("identifier"));
        IWebElement GoogPsw => driver.FindElement(By.Name("password"));

        public void GoogleLogin(string eml, string pass)
        {
            LoginBut.Click();
            Thread.Sleep(3422);
            GoogLogIn.Click();
            GoogEml.SendKeys(eml + Keys.Enter);
            Thread.Sleep(1272);
            GoogPsw.SendKeys(pass + Keys.Enter);
            Thread.Sleep(6284);
        }
    }
}
