﻿using AventStack.ExtentReports;
using MagColTest.Other;
using MagColTest.Other.DataMaps;
using NUnit.Framework;
using NUnit.Framework.Interfaces;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Props = MagColTest.Properties.Params;

namespace MagColTest.Tests
{
    [TestFixture]
    public class Test : Base
    {

        [Test]
        public void Scenario2()
        {
            // Get JSON data
            string json_data = "";
            using (StreamReader sr = new StreamReader(@"C:\Users\jucan\source\repos\MagColTest\MagColTest\TestData\TestMessages.json"))
            {
                json_data = sr.ReadToEnd();
            }

            MessageData md = Utils.DeserializeJson(json_data);            

            // Page instances
            Pages.LoginPage Login = new Pages.LoginPage(driver);
            Pages.CustomerAccount.AdreseleMele adr = new Pages.CustomerAccount.AdreseleMele(driver);

            // SQL stuff
            SqlCommand cmd = new SqlCommand("SELECT * FROM [testDb].[dbo].[CredentialsX] WHERE [accountType] = 'Google'", conn);
            SqlDataReader reader = cmd.ExecuteReader();
            if (reader.Read())
            {     
                Login.GoogleLogin(reader.GetString(1), reader.GetString(2));
            }

            var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(12));
            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.LinkText("Adresele mele")));

            Assert.AreEqual(md.AddOrEditAdreseleMele.InfoMessages.GoogleLoginSuccess, adr.SuccessMessage_GoogleLogin());
            test.Log(Status.Info, adr.SuccessMessage_GoogleLogin());

            adr.SelectOption();
            adr.DateContact("Test pren", "Test num", "Test soc", 938593335, 583850000);
            adr.Adresa("Test adresa 1", "Test adresa 2", "Localitate", "Buc", "061660", "Rom");

            adr.SaveDetails();

            Assert.AreEqual(md.AddOrEditAdreseleMele.InfoMessages.AddressSaveSuccess, adr.SuccessMessage_Save());
            test.Log(Status.Info, adr.SuccessMessage_Save());

            reader.Close();
        }

        [Test]
        public void TestingSearch()
        {
            Pages.SearchResults sr = new Pages.SearchResults(driver);

            string searchFor = "car";
            sr.SearchAndSubmit(searchFor);

            int items = sr.SeachItemCount();
            int pageItems = sr.PageSearchItemCount();

            test.Log(Status.Info, String.Format(
                "The search for '{0}' had {1} results and displays {2} items in page.",
                searchFor,
                pageItems,
                items));

            try
            {
                Assert.GreaterOrEqual(items, pageItems);
            }
            catch (AssertionException)
            {
                test.Log(Status.Fail, String.Format("The amount of items resulted in page is less than the amount of items displayed."));
            }

        }

        [Test]
        public void Scenario1()
        {
            // Select which product to use from search
            int SelectedItem = 0;

            // Get JSON data
            string json_data = "";
            using (StreamReader sr = new StreamReader(@"C:\Users\jucan\source\repos\MagColTest\MagColTest\TestData\TestMessages.json"))
            {
                json_data = sr.ReadToEnd();
            }

            MessageData md = Utils.DeserializeJson(json_data);

            // Page instances
            Pages.LoginPage Login = new Pages.LoginPage(driver);
            Pages.SearchResults Search = new Pages.SearchResults(driver);
            Pages.ProductPage Product = new Pages.ProductPage(driver);
            Pages.CustomerAccount.Favorite Favs = new Pages.CustomerAccount.Favorite(driver);

            // SQL Login
            SqlCommand cmd = new SqlCommand("SELECT * FROM [testDb].[dbo].[CredentialsX] WHERE [accountType] = 'Google'", conn);
            SqlDataReader reader = cmd.ExecuteReader();

            if (reader.Read())
            {
                Login.GoogleLogin(reader.GetString(1), reader.GetString(2));
            }
            test.Log(Status.Info, md.AddOrEditAdreseleMele.InfoMessages.GoogleLoginSuccess);
            // End of Login

            Search.SearchAndSubmit("fiat");
            string Item = Search.ProductName(SelectedItem);
            test.Log(Status.Info, String.Format("Selected item for this test is <i>'{0}'</i>", Item));

            Search.SelectItem(SelectedItem);
            StringAssert.AreEqualIgnoringCase(Item, Product.ProductTitle());

            Product.AddToFavorites();
            StringAssert.Contains(Item, Favs.SuccessMessage());
            StringAssert.AreEqualIgnoringCase(Item, Favs.FavoritesList(SelectedItem));
            test.Log(Status.Info, String.Format("<i>'{0}'</i> was added successfully to Favorites!", Item));

            Favs.RemoveFromFavoriteList(SelectedItem);
            IAlert alert = driver.SwitchTo().Alert();
            alert.Accept();
            test.Log(Status.Info, String.Format("<i>'{0}'</i> was removed successfully from Favorites!", Item));
            StringAssert.AreEqualIgnoringCase("Nu ai nimic in lista de favorite.", Favs.WhishListEmptyMessage());

            reader.Close();
        }


    }

}