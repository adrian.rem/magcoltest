﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MagColTest.Pages
{
    public class ProductPage
    {
        IWebDriver driver;

        public ProductPage(IWebDriver driver)
        {
            this.driver = driver;
        }
        
        IWebElement PrdctTtl => driver.FindElement(By.TagName("h1"));

        IWebElement FavLnk => driver.FindElement(By.LinkText("Adauga la favorite"));

        public void AddToFavorites()
        {
            FavLnk.Click();
        }

        public string ProductTitle()
        {
            return PrdctTtl.Text;
        }
    }
}
